<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function daftar()
    {
        return view('daftar');
    }

    public function data_alumni()
    {
        return view('dataalumni');
    }

    public function kontak_kami()
    {
        return view('kontak_kami');
    }
}
