<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/daftar', 'HomeController@daftar')->name('daftar');
Route::get('/data-alumni', 'HomeController@data_alumni')->name('data-alumni');
Route::get('/kontak-kami', 'HomeController@kontak_kami')->name('kontak-kami');
