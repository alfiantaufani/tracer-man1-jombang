@extends('layouts.app')
@section('content')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">
        <ol>
          <li><a href="{{ url('/') }}">Home</a></li>
          <li>Daftar</li>
        </ol>
        <h2>Daftar Alumni</h2>
      </div>
    </section><!-- End Breadcrumbs -->

    <section id="counts" class="counts">
        <div class="container" data-aos="fade-up">
          <div class="count-box">
            <section id="tabs" class="project-tab">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <nav>
                                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">DATA DIRI SISWA</a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">DATA PEKERJAAN</a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">PENDIDIKAN LANJUTAN</a>
                                </div>
                            </nav>
                            <form>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                        <div class="row mt-4">
                                            <div class="col-lg-6">
                                                <div class="form-group row">
                                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Nama</label>
                                                    <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Masukkan Nama Lengkap">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Tahun Masuk</label>
                                                    <div class="col-sm-8">
                                                    <input type="number" class="form-control" id="inputPassword3" placeholder="Masukkan Tahun Masuk">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Tahun Lulus</label>
                                                    <div class="col-sm-8">
                                                    <input type="number" class="form-control" id="inputPassword3" placeholder="Masukkan Tahun Lulus">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Jurusan</label>
                                                    <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="inputPassword3" placeholder="Masukkan Jurusan">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Email</label>
                                                    <div class="col-sm-8">
                                                    <input type="email" class="form-control" id="inputPassword3" placeholder="Masukkan Email">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">No. Whatsapp</label>
                                                    <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="inputPassword3" placeholder="Masukkan No. Whatsapp">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Alamat Domisili</label>
                                                    <div class="col-sm-8">
                                                    <textarea type="text" class="form-control" row="3"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-4 col-form-label">Alamat</label>
                                                    <div class="col-sm-8">
                                                    <textarea type="text" class="form-control" row="3"></textarea>
                                                    </div>
                                                </div>
                                                
                                                
                                            </div>
                                            <div class="form-group row mt-2">
                                                <div class="col-sm-10">
                                                </div>
                                                <div class="col-sm-2" style="float: right">
                                                    <button type="submit" class="btn btn-outline-success text-right">Prev</button>
                                                    <button type="submit" class="btn btn-success text-right">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                        <div class="row mt-4">
                                            <div class="col-lg-12">
                                                <div class="form-group row">
                                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Pekerjaan Sekarang <small>(opsional)</small></label>
                                                    <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Masukkan Pekerjaan Sekarang">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-3 col-form-label">Nama Perusahaan <small>(opsional)</small></label>
                                                    <div class="col-sm-9">
                                                    <input type="number" class="form-control" id="inputPassword3" placeholder="Masukkan Nama Perusahaan">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-3 col-form-label">Jabatan <small>(opsional)</small></label>
                                                    <div class="col-sm-9">
                                                    <input type="number" class="form-control" id="inputPassword3" placeholder="Masukkan Jabatan">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-3 col-form-label">Alamat Perusahaan <small>(opsional)</small></label>
                                                    <div class="col-sm-9">
                                                    <textarea type="text" class="form-control" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row mt-2">
                                                <div class="col-sm-10">
                                                </div>
                                                <div class="col-sm-2" style="float: right">
                                                    <button type="submit" class="btn btn-outline-success text-right">Prev</button>
                                                    <button type="submit" class="btn btn-success text-right">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                        <div class="row mt-4">
                                            <div class="col-lg-12">
                                                <div class="form-group row">
                                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Universitas <small>(opsional)</small></label>
                                                    <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Masukkan Pekerjaan Sekarang">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-3 col-form-label">Semester <small>(opsional)</small></label>
                                                    <div class="col-sm-9">
                                                    <input type="number" class="form-control" id="inputPassword3" placeholder="Masukkan Nama Perusahaan">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-3 col-form-label">Jurusan <small>(opsional)</small></label>
                                                    <div class="col-sm-9">
                                                    <input type="number" class="form-control" id="inputPassword3" placeholder="Masukkan Jabatan">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-3 col-form-label">Foto</label>
                                                    <div class="col-sm-9">
                                                        <div class="form-group files">
                                                            <input type="file" class="form-control" multiple="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row mt-2">
                                                <div class="col-sm-10">
                                                </div>
                                                <div class="col-sm-2" style="float: right">
                                                    <button type="submit" class="btn btn-outline-success text-right">Prev</button>
                                                    <button type="submit" class="btn btn-success text-right">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
          </div>
        </div>
    </section>

</main>

@endsection