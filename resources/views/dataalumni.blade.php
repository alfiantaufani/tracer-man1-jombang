@extends('layouts.app')
@section('content')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{ url('/') }}">Home</a></li>
          <li>Data Alumni</li>
        </ol>
        <h2>Data Alumni</h2>

      </div>
    </section><!-- End Breadcrumbs -->
    <footer id="footer" class="footer" style="padding: 0px;">
      <div class="footer-newsletter" style="padding: 0px; padding-bottom: 50px;">
        <div class="container">
          <section id="counts" class="counts">
            <div class="container" data-aos="fade-up">
              <h4><center>Data Alumni</center></h4><br>
              <div class="row gy-4">
                <div class="col-lg-4 col-md-6">
                  <div class="count-box">
                    <img src="{{ asset('img/statistics.png') }}" class="" width="50" alt="">
                    <div style="margin-left : 15px;">
                      <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" class="purecounter"></span>
                      <p>Jumlah Data Alumni</p>
                    </div>
                  </div>
                </div>
      
                <div class="col-lg-4 col-md-6">
                  <div class="count-box">
                    <img src="{{ asset('img/company.png') }}" class="" width="50" alt="">
                    <div style="margin-left : 15px;">
                      <span data-purecounter-start="0" data-purecounter-end="521" data-purecounter-duration="1" class="purecounter"></span>
                      <p>Jumlah Yang Bekerja</p>
                    </div>
                  </div>
                </div>
      
                <div class="col-lg-4 col-md-6">
                  <div class="count-box">
                    <img src="{{ asset('img/scientist.png') }}" class="" width="50" alt="">
                    <div style="margin-left : 15px;">
                      <span data-purecounter-start="0" data-purecounter-end="1463" data-purecounter-duration="1" class="purecounter"></span>
                      <p>Jumlah Lanjut Pendidikan</p>
                    </div>
                  </div>
                </div>
      
              </div>
             </div>
          </section><!-- End Counts Section -->
    
          <div class="row justify-content-center mt-5">
            <div class="col-lg-12 text-center">
              <h4>Cari Nama Alumni Disini</h4>
              <p>Masukkan nama siswa yang akan kamu cari.</p>
            </div>
            <div class="col-lg-10">
              <form action="" method="post">
                <input type="text" name="cari" placeholder="Masukkan Nama"><input type="submit" value="Cari">
              </form>
            </div>
          </div>
          <section id="counts" class="counts">
            <div class="container" data-aos="fade-up">
              <div class="count-box">
                  <div class="table-responsive">
                        <table id="table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                                <th>Extn.</th>
                                <th>E-mail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Tiger</td>
                                <td>Nixon</td>
                                <td>System Architect</td>
                                <td>Edinburgh</td>
                                <td>61</td>
                                <td>2011/04/25</td>
                                <td>$320,800</td>
                                <td>5421</td>
                                <td>t.nixon@datatables.net</td>
                            </tr>
                            <tr>
                                <td>Donna</td>
                                <td>Snider</td>
                                <td>Customer Support</td>
                                <td>New York</td>
                                <td>27</td>
                                <td>2011/01/25</td>
                                <td>$112,000</td>
                                <td>4226</td>
                                <td>d.snider@datatables.net</td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
              </div>
            </div>
          </section>

        </div>
      </div>
    </footer>
   
  </main><!-- End #main -->

@endsection